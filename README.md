This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[![Netlify Status](https://api.netlify.com/api/v1/badges/8984cff3-172d-4a0f-b9db-b01590e2e5b7/deploy-status)](https://app.netlify.com/sites/blissful-babbage-71d3f7/deploys)

## Known Bug

- Pagination issue after choosing category
