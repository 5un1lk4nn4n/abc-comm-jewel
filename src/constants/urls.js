// export const BASE_URL = 'http://192.168.43.137:8000/api/jewels/';
// export const ACCOUNT_URL = 'http://192.168.43.137:8000/api/accounts/';

// DEMO API URL
export const BASE_URL = 'https://sunilv.pythonanywhere.com/api/jewels/';
export const ACCOUNT_URL = 'https://sunilv.pythonanywhere.com/api/accounts/';

export const URLS = {
  login: 'auth/login',
  products: 'products',
  categories: 'categories',
  wishlists: 'wishlists',
  dashboard: 'dashboard',
};
