import * as React from 'react';
import { Box, Heading, Paragraph, Text, Button } from 'grommet';
import { Gitlab, Home } from 'react-feather';

/**
 * About Page gives technical information
 */

const About = () => (
  <Box margin="large">
    <Heading>ABC Commerce</Heading>
    <Heading level="4">Technical Information</Heading>
    <Box>
      <Box
        pad="small"
        border={{
          side: 'top',
        }}
      >
        <Paragraph>FrontEnd</Paragraph>
        <Text>Reactjs - 16.13.1</Text>
        <Text>Gommet - UI React Framework</Text>
      </Box>
      <Box
        pad="small"
        border={{
          side: 'top',
        }}
      >
        <Paragraph>BackEnd</Paragraph>
        <Text>Django Rest Framework - 3.11</Text>
        <Text>SQL Lite DB</Text>
      </Box>
      <Box pad="small" direction="row-responsive" gap="small">
        <Button
          label="Backend Django Source Code"
          primary
          color="black"
          icon={<Gitlab />}
          target="_blank"
          href="https://gitlab.com/sunilv/abc-comm-jewel-backend"
        />
        <Button
          label="Frontend React Source Code"
          primary
          color="black"
          icon={<Gitlab />}
          target="_blank"
          href="https://gitlab.com/sunilv/abc-comm-jewel"
        />
        <Button label="Back To Home" primary color="black" icon={<Home />} href="/" />
      </Box>
    </Box>
  </Box>
);

export default About;
