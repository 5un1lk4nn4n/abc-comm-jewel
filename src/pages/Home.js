import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Filter, Frown, Plus, XCircle, X } from 'react-feather';
import { Box, Button, Heading, Layer } from 'grommet';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Pagination from '../components/Pagination';
import Card from '../components/Card';
import Loader from '../components/Loader';
import apiService from '../services/api';

/**
 * Home Page.
 *
 */

class Home extends Component {
  topRef = React.createRef();
  filterRef = React.createRef();
  api = new apiService();
  maxPages = 0;
  perPage = 10;

  constructor(props) {
    super(props);
    this.state = {
      selectedCategories: [],
      plist: [],
      catList: [],
      loading: true,
      wishlist: null,
      maxPages: 0,
    };
  }

  componentDidMount() {
    this.getProducts();
    this.getWishList();
    this.getCategories();
  }

  /* Remove Wishlist using wish id */
  removeWishList = (wishId) => {
    this.api
      .removeWishList(wishId)
      .then((res) => {
        if (res.code === 200) {
          this.getWishList();
        }
      })
      .catch((error) => {});
  };

  /* get Wishlist */
  getWishList = () => {
    this.api
      .getWishlist()
      .then((res) => {
        if (res.code === 200) {
          this.setState({
            wishlist: res,
          });
        }
      })
      .catch((error) => {});
  };

  /* get categories */
  getCategories = (page = 1, perPage = 20) => {
    this.api
      .getCategories(page, perPage)
      .then((res) => {
        if (res.code === 200) {
          this.setState({
            catList: res.data,
          });
        }
      })
      .catch((error) => {});
  };

  /**  get filtered categories product list. called when model is closed */
  filterProductByCategories = () => {
  
    this.getProducts(1, this.perPage);
  };

  renderCategoryButtons = (isSelected, cat, key) => {
    return (
      <Button
        margin="small"
        style={{
          borderRadius: '2px',
        }}
        label={cat.label}
        primary
        onClick={() => {
          if (isSelected) {
            this.removeSelectedCategories(cat);
          } else {
            this.setSelectedCategories(cat);
          }
        }}
        icon={isSelected ? <XCircle /> : <Plus />}
        reverse
        key={isSelected ? `rm-btn-cat-${key}` : `add-btn-cat-${key}`}
      />
    );
  };

  /* Get Product list. filter is category filter */
  getProducts = (page = 1, perPage = 10) => {
    const { selectedCategories } = this.state;
    let cat = []
    if(selectedCategories.length){
    	cat = selectedCategories.map((cat) => cat.id);
    	
    }
    this.api
      .getProducts(page, perPage,cat)
      .then((res) => {
        if (res.code === 200) {
          this.maxPages = res.max_pages;
          this.setState(
            {
              loading: false,
              plist: res.data,
              maxPages: res.max_pages,
            },
            () => {
              this.topRef.current.scrollIntoView({ behavior: 'smooth' });
            },
          );
        }
      })
      .catch((error) => {});
  };

  /* Adding Choosen categories to array for filtering */
  setSelectedCategories = (selectedCategory) => {
    this.setState((prevState) => ({
      selectedCategories: [...prevState.selectedCategories, selectedCategory],
    }));
  };

  /* Adding Choosen categories to array for filtering */
  removeSelectedCategories = (removedCategory) => {
    const { selectedCategories } = this.state;
    const filtered = selectedCategories.filter(function (el) {
      return el.id !== removedCategory.id;
    });
    this.setState(() => ({
      selectedCategories: filtered,
    }));
  };

  isCategorySelected = (category) => {
    const { selectedCategories } = this.state;
    const isCategoryFound = (obj) => obj.id === category;
    return selectedCategories.some(isCategoryFound);
  };

  /* Add Wishlist */
  addToWishlist = (productId) => {
    this.api
      .addToWishlist(productId)
      .then((res) => {
        if (res.code === 200) {
          this.getWishList();
        }
      })
      .catch((error) => {});
  };

  /** Check if product is alreact in wishlist. Note: Currently checking in local array only. */
  isInWishList = (productId) => {
    const { wishlist } = this.state;
    if (wishlist && wishlist.count) {
      const res = wishlist.data.filter((product) => product.product === productId);
      return res.length ? true : false;
    }
    return false;
  };

  /** Render Product card */
  renderProduct = () => {
    const { plist } = this.state;
    if (plist.length) {
      return plist.map((product, key) => {
        const { description, image, category_name, collection_name, id } = product;
        const isInWishList = this.isInWishList(id);
        return (
          <Card
            key={`product-card-${key}`}
            description={description}
            image={image}
            category={category_name}
            collection={collection_name}
            productId={id}
            addToWishlist={this.addToWishlist}
            isInWishList={isInWishList}
          />
        );
      });
    }

    /** Empty Product view */
    return (
      <Box align="center" justify="center" height="medium">
        <Frown />
        <Heading level={5}>No Products Available</Heading>
      </Box>
    );
  };

  /** Pagination on Page request func */
  onPageChanged = (page) => {
    this.getProducts(page.currentPage);
  };
  
  closeLayer = () => {
  
   this.setState({ showFilter: false }, () => {
                  this.filterProductByCategories();
                });
  }

  render() {
    const { loading, wishlist, catList, showFilter, selectedCategories } = this.state;
    return (
      <Box ref={this.topRef}>
        <Header wishlist={wishlist} removeWishList={this.removeWishList} />
        <Box
          direction="row"
          gap="small"
          justify="end"
          align="center"
          margin={{
            top: '100px',
            right: 'medium',
          }}
        >
          <Button
            icon={<Filter />}
            color="black"
            label="Filter by Catergories"
            ref={this.filterRef}
            onClick={() => {
              this.setState({ showFilter: true });
            }}
          />
          {showFilter && (
            <Layer
              position="right"
              full="vertical"
              modal={true}
              onEsc={this.closeLayer}
              onClickOutside={this.closeLayer}
            >
              <Box pad="small" width="large" style={{position:'relative'}}>
              	<Button icon={<X/>} onClick={this.closeLayer} style={{ position:"absolute", top:10, left:10}} /> 
              	<Box margin="small"/>
                <Heading level={4}>Select Categories to Filter Your Favorites!</Heading>
                {
                selectedCategories.length ? <> <Heading level={6}>Chosen Categories:</Heading>
                <Box pad="small" gap="small" direction="row" wrap>
                  {selectedCategories.map((cat, key) => this.renderCategoryButtons(true, cat, key))}
                </Box></> : null
                
                }
               
                <Heading level={6}>All Categories</Heading>
                <Box pad="small" gap="small">
                  {catList.map((cat, key) => {
                    return this.isCategorySelected(cat.id)
                      ? null
                      : this.renderCategoryButtons(false, cat, key);
                  })}
                </Box>
              </Box>
            </Layer>
          )}
        </Box>
        <Box height={loading && !this.maxPages ? `${window.innerHeight - 200}px` : 'auto'}>
          {loading ? (
            <Loader />
          ) : (
            <Box>
              <Box direction="row-responsive" justify="center" align="center" margin="medium" wrap>
                {this.renderProduct()}
              </Box>
              {this.maxPages ? (
                <Box height="small">
                  <Pagination
                    totalRecords={this.perPage * this.state.maxPages}
                    pageLimit={this.perPage}
                    pageNeighbours={3}
                    onPageChanged={this.onPageChanged}
                  />
                </Box>
              ) : null}
            </Box>
          )}
        </Box>

        <Footer />
      </Box>
    );
  }
}

export default withRouter(Home);
