import React, { useState, useCallback, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { useDropzone } from 'react-dropzone';
import csv from 'csv';
import { ArrowLeft, AlertCircle } from 'react-feather';

import { Box, Text, Anchor, Header, Heading } from 'grommet';
import Footer from '../components/Footer';
import ApiService from '../services/api';

const api = new ApiService();
var errorList = [];

const ManageProduct = (props) => {
  const [message, setMessage] = useState();
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dashboard, setDashboard] = useState();
  console.log(dashboard);
  const getDashboard = () => {
    api
      .getDashboard()
      .then((res) => {
        if (res.code === 200) {
          setDashboard(res.data.count);
        }
      })
      .catch((error) => {});
  };

  useEffect(() => {
    getDashboard();
  }, []);

  const onDrop = useCallback((acceptedFiles) => {
    errorList = [];
    if (acceptedFiles.length > 1) {
      setMessage('Multiple files unsupported');
      setError(true);
      return;
    }
    const file = acceptedFiles[0];

    const extension = file.name.substring(file.name.lastIndexOf('.') + 1);
    const size = file.size;
    if (extension !== 'csv' || size > 10485760) {
      setMessage('Only CSV file with less than 10 mb is supported.');
      setError(true);
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      setLoading(true);
      setError(false);
      setMessage('Please Your file is processed..');
      csv.parse(reader.result, (err, data) => {
        setMessage(
          `Your file has ${data.length} records and being processed in server. Please wait..`,
        );
        const intervalId = setInterval(getDashboard, 1000);
        api
          .uploadProducts(data)
          .then((res) => {
            if (res.code === 200) {
              setMessage(res.message);
              setLoading(false);
            }
            if (res.code === 400) {
              errorList = res.error;
              setMessage(res.message);
              setLoading(false);
              setError(true);
            }
            clearInterval(intervalId);
          })
          .catch((error) => {});
      });
    };
    reader.readAsBinaryString(file);
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
  });

  const renderError = () => {
    return errorList.map((err, key) => {
      if (!err.code) {
        return null;
      }
      return (
        <Box
          direction="row"
          gap="small"
          margin={{
            vertical: '1px',
          }}
        >
          <Text>{`Row ${key}:`}</Text>
          <Text>{err.code[0]}</Text>
        </Box>
      );
    });
  };

  return (
    <Box>
      <Header
        pad="small"
        background="brand"
        border={{
          color: 'brand',
          side: 'bottom',
        }}
      >
        <Anchor color="white" icon={<ArrowLeft />} label="Back to Home" href="/" />
        <Text color="white">Product Management Center</Text>
      </Header>
      <Box>
        <Box direction="row-responsive" align="center" justify="center">
          <Box
            background="tertiary"
            width="small"
            pad="small"
            alignSelf="center"
            align="center"
            margin="large"
            border={{
              side: 'bottom',
              size: 'medium',
              color: 'brand',
            }}
          >
            <Heading level={5}>Products Available</Heading>
            <Text weight="bold" size="large">
              {dashboard ? dashboard.product : 0}
            </Text>
          </Box>
          <Box
            background="tertiary"
            width="small"
            pad="small"
            alignSelf="center"
            align="center"
            margin="small"
            border={{
              side: 'bottom',
              size: 'medium',
              color: 'brand',
            }}
          >
            <Heading level={5}>Categories Available</Heading>
            <Text weight="bold" size="large">
              {dashboard ? dashboard.category : 0}
            </Text>
          </Box>
        </Box>

        <Box
          align="center"
          margin="large"
          {...getRootProps()}
          border={{
            style: 'dashed',
          }}
        >
          <input {...getInputProps()} />
          {isDragActive ? (
            <p>Drop the files here ...</p>
          ) : (
            <p>
              Drag 'n' drop or click to select product file.{' '}
              <strong>Note: Only CSV and with size less than 10mb is accepted. </strong>
            </p>
          )}
        </Box>
        <Box alignSelf="center">
          {error && message ? (
            <Box background="status-error" pad="small" direction="row" align="center" gap="small">
              <AlertCircle />
              {message}
            </Box>
          ) : null}

          {loading ? (
            <Box
              background="status-ok"
              pad="small"
              direction="row"
              align="center"
              gap="small"
              animation={{
                type: 'pulse',
                duration: 250,
              }}
            >
              {message}
            </Box>
          ) : null}

          {!loading && !error && message ? (
            <Box background="status-ok" pad="small" direction="row" align="center" gap="small">
              {message}
            </Box>
          ) : null}
          {errorList.length ? (
            <Box margin="small" fill>
              {renderError()}{' '}
            </Box>
          ) : null}
        </Box>
      </Box>
      <Footer />
    </Box>
  );
};

export default withRouter(ManageProduct);
