import React, { lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Grommet, Box } from 'grommet';
import { grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';
import styled from 'styled-components';

import { THEME } from './styles';
import Loader from './components/Loader';
import NotFound from './pages/PageNotFound';

const Home = lazy(() => import('./pages/Home'));
const ManageProduct = lazy(() => import('./pages/ManageProduct'));
const About = lazy(() => import('./pages/About'));

const customTheme = deepMerge(grommet, THEME.PROPS);

const MainContainer = styled(Box)`
  min-height: 100vh;
  overflow: hidden;
  display: block;
  position: relative;
  padding-bottom: 100px;
`;

const Main = () => (
  <MainContainer>
    <Grommet full theme={customTheme}>
      <Router>
        <Suspense fallback={<Loader />}>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/manage-product" exact component={ManageProduct} />
            <Route path="/about" exact component={About} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Suspense>
      </Router>
    </Grommet>
  </MainContainer>
);

ReactDOM.render(<Main />, document.getElementById('root'));
