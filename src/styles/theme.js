import { css } from 'styled-components';

import { primary, secondary, tertiary, background } from './colors';

const APP_COLORS = {
  brand: primary,
  primary,
  secondary,
  tertiary,
  background,
};

export const PROPS = {
  global: {
    colors: APP_COLORS,
    font: {
      size: '15px',
    },
    edgeSize: {
      small: '10px',
    },
    elevation: {
      light: {
        small: '0px 1px 5px rgba(0, 0, 0, 0.50)',
        medium: '0px 3px 8px rgba(0, 0, 0, 0.50)',
      },
    },
  },
  text: {
    medium: {
      size: '15px',
    },
  },
  tab: {
    active: {
      background: 'primary',
      color: 'white',
    },
    background: 'white',
    border: undefined,
    color: 'brand',
    hover: {
      background: 'primary',
      color: 'white',
    },
    margin: undefined,
    pad: {
      vertical: 'xsmall',
      horizontal: 'small',
    },
    extend: ({ theme }) => css`
      border-radius: 5px;
    `,
  },
  tabs: {
    background: 'background',

    gap: 'medium',
    margin: {
      medium: 'small',
    },
    header: {
      background: 'background',
      extend: ({ theme }) => css`
        padding: ${theme.global.edgeSize.small};
      `,
    },
    panel: {
      extend: ({ theme }) => css`
        padding: 0;
      `,
    },
  },
};
