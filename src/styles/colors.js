export const base = '#20e1a5';
export const primary = '#20e1a5';
export const secondary = '#F9D342';
export const tertiary = '#f4f5f8';
export const background = '#fff';
export const white = '#fff';
export const black = '#444444';
