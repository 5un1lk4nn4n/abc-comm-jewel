import React from 'react';
import { Box, Text, Footer as Foot } from 'grommet';

import styled from 'styled-components';

const BottomFooter = styled(Foot)`
  bottom: 0;
  height: 50px;
  left: 0;
  position: absolute;
  width: 100%;
`;

/*** Simple Footer Page */
const Footer = () => {
  return (
    <BottomFooter
      direction="row-responsive"
      border={{
        side: 'top',
        color: 'brand',
      }}
      justify="center"
      fill
      pad="medium"
    >
      <Box direction="row" align="center">
        <Text color="brand" weight="bold">
          &copy; ABC Commerce 2020
        </Text>
      </Box>
    </BottomFooter>
  );
};

export default Footer;
