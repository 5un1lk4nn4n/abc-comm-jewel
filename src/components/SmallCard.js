import React from 'react';
import { Box, Button, Image, Text } from 'grommet';
import PropTypes from 'prop-types';
import { Trash } from 'react-feather';

/**
 *  Product Wish list card shown on pop up
 * */

const SmallCard = ({ wishId, category, description, image, removeWishlist }) => {
  return (
    <Box height="100px" direction="row-responsive" gap="small" margin="small" justify="around">
      <Box direction="row-responsive" gap="small" align="center">
        <Box width="50px" height="50px">
          <Image src={image} fit="contain" />
        </Box>
        <Box>
          <Text size="small">{description ? description : 'No description available'}</Text>
          <Text size="small">{category}</Text>
        </Box>
      </Box>
      <Box
        direction="row"
        border={{
          side: 'bottom',
        }}
      >
        <Button icon={<Trash color="red" size="15" />} onClick={() => removeWishlist(wishId)} />
      </Box>
    </Box>
  );
};

SmallCard.propTypes = {
  category: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  removeWishlist: PropTypes.func.isRequired,
};

export default SmallCard;
