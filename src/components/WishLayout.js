import React from 'react';
import { Box, Heading, Button } from 'grommet';
import PropTypes from 'prop-types';
import { Heart, X } from 'react-feather';
import SmallCard from '../components/SmallCard';

const WishLayout = ({ wishlist, removeWishlist, setShowLayer }) => {
  return (
    <Box width="medium">
      <Box
        align="center"
        background="brand"
        pad="medium"
        direction="row"
        justify="center"
        gap="small"
        style={{
        position:"relative",
        }}
      >
      	<Button icon={<X color="white"/>} onClick={()=>{setShowLayer(false)}} style={{
      	position:"absolute",
      	top:10,
      	right:10
      	}} />
        <Heart color="white" size="25" />
        <Heading color="white" level="3">
          Your Wishlist
        </Heading>
      </Box>
      <Box>
        {!wishlist ? (
          <Box fill justify="center" align="center" pad="small">
            <Heading level={5}>Empty</Heading>
          </Box>
        ) : (
          wishlist.data.map((prod, key) => (
            <SmallCard
              key={`wish-id-${key}`}
              wishId={prod.id}
              category={prod.category_name}
              description={prod.description}
              image={prod.image}
              removeWishlist={removeWishlist}
            />
          ))
        )}
      </Box>
    </Box>
  );
};

WishLayout.propTypes = {
  wishlist: PropTypes.shape({
    category: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
  }),
  removeWishlist: PropTypes.func.isRequired,
};

export default WishLayout;
