import * as React from 'react';
import { Anchor, Box, Heading } from 'grommet';
import PropTypes from 'prop-types';
import { AlertCircle } from 'react-feather';

const messages = {
  404: 'Sorry, Page Not Found',
  500: 'There was error in Request. Please try again or contact Help & Support',
  unknown: 'Unknown Issue.Contact Help & Support',
};

/***
 * Error Page such as Page not found or others
 */
const ErrorPage = ({ code }) => (
  <Box fill align="center" justify="center">
    <AlertCircle size={40} />
    <Heading level={2}>{messages[code]}</Heading>
    <Anchor href="/" label="Back to home" size="medium" />
  </Box>
);

ErrorPage.propTypes = {
  code: PropTypes.oneOf(['404', '500', 'Unknown']),
};

ErrorPage.defaultProps = {
  code: '404',
};
export default ErrorPage;
