import React from 'react';
import { Box, Text } from 'grommet';
import { ShoppingBag } from 'react-feather';

/*** Loader with animation and icon */
const Loader = () => {
  return (
    <Box background="white" align="center" justify="center" pad="medium" fill>
      <Box
        align="center"
        justify="center"
        animation={{
          type: 'pulse',
          duration: 250,
        }}
      >
        <ShoppingBag />
        <Text>ABC Commerce</Text>
      </Box>
    </Box>
  );
};

export default Loader;
